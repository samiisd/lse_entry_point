#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <time.h>
#include <limits.h>
#include <string.h>
#include <err.h>
#include <assert.h>
#include <immintrin.h>
#include <omp.h>

#define ADD(a, b) (_mm256_add_ps((a), (b)))
#define SUB(a, b) (_mm256_sub_ps((a), (b)))
#define MUL(a, b) (_mm256_mul_ps((a), (b)))
#define DIV(a, b) (_mm256_div_ps((a), (b)))

#define IO_BUFFER_SIZE (1 << 20) // 1 MB

struct kmean_data
{
    float *data;

    size_t size;

    unsigned nb_vec;
    unsigned nb_features;

    unsigned padding;
};

static inline unsigned calculate_padding(unsigned size, unsigned alignment)
{
    if (size == alignment)
        return 0;
    return ((size & (~alignment + 1)) + alignment) - size;
}

struct kmean_data loadData(char *fileName, unsigned nbVec, unsigned dim) {
    struct kmean_data data_info = {0};
    data_info.nb_vec = nbVec;
    data_info.nb_features = dim;
    data_info.padding = calculate_padding(dim, 32);

    data_info.size = nbVec * (dim + data_info.padding) * sizeof(float);

    FILE *fp = fopen(fileName, "r");
    if (!fp) {
           printf("File not Found: %s\n", fileName);
               exit(1);
    }

    data_info.data = aligned_alloc(32, data_info.size);
    memset(data_info.data, 0, data_info.size);

    float *data_iterator = data_info.data;
    float *data_end = data_iterator + nbVec * (dim + data_info.padding);

    const size_t real_buf_size = IO_BUFFER_SIZE - (IO_BUFFER_SIZE % dim);

    size_t nb_vec_read = 0;
    float buffer[real_buf_size];
    while (data_iterator != data_end) {
        size_t nb_read = fread(buffer, sizeof(float), real_buf_size, fp);

        assert(nb_read != 0);
        assert(nb_read % dim == 0);

        for (unsigned i = 0; i < nb_read && data_iterator != data_end; i += dim)
        {
            memcpy(data_iterator, buffer + i, dim * sizeof(float));
            data_iterator += dim + data_info.padding;

            nb_vec_read++;
        }


    }

    assert(nb_vec_read == nbVec);
    assert(data_iterator == data_end);

    fclose(fp);

    return data_info;
}

void writeClassinFloatFormat(unsigned char *data, unsigned nbelt, char *fileName) {
    FILE *fp = fopen(fileName, "w");
    if (!fp) {
        printf("Cannot create File: %s\n", fileName);
        exit(1);
    }

    for(unsigned i = 0; i < nbelt; ++i) {
        float f = data[i];
        fwrite(&f, sizeof(float), 1, fp);
    }
    fclose(fp);
}

static inline double sum8(__m256 x) {
    float dist__[8] __attribute__ ((aligned (32)));
    _mm256_store_ps(dist__, x);

    double res = 0;
    #pragma omp simd
    for (unsigned char i = 0; i < 8; ++i)
        res += dist__[i];

    return res;
}

unsigned char *Kmeans(struct kmean_data *data_info, unsigned char K, double minErr, unsigned maxIter)
{
    const float *data = __builtin_assume_aligned(data_info->data, 32);
    const unsigned dim = data_info->nb_features;
    const unsigned nbVec = data_info->nb_vec;
    const unsigned padding = data_info->padding;

    unsigned iter = 0;
    double diffErr = DBL_MAX, Err = DBL_MAX;

    float *means = __builtin_assume_aligned(
        aligned_alloc(32, (dim + padding) * K * sizeof(float)),
        32);

    float *means_non_normalized = __builtin_assume_aligned(
        aligned_alloc(32, (dim + padding) * K * sizeof(float)),
        32);

    unsigned *card = calloc(K, sizeof(unsigned));
    unsigned char* c = malloc(sizeof(unsigned char) * nbVec);

    memset(means, 0, (dim + padding) * K * sizeof(float));

    // Random init of c
    for(unsigned i = 0; i < nbVec; ++i)
        c[i] = rand() / (RAND_MAX + 1.) * K;

    for(unsigned i = 0; i < nbVec; ++i) {
        float *means_iterator = __builtin_assume_aligned(means + c[i] * (dim + padding), 32);
        float *data_iterator = __builtin_assume_aligned(data + i * (dim + padding), 32);

        #pragma omp simd
        for(unsigned j = 0; j < dim; ++j)
            means_iterator[j] += data_iterator[j];
        ++card[c[i]];
    }

    memcpy(means_non_normalized, means, (dim + padding) * K * sizeof(float));

    for(unsigned i = 0; i < K; ++i) {
        float *means_iterator = __builtin_assume_aligned(means + i * (dim + padding), 32);
        for(unsigned j = 0; j < dim; ++j)
            means_iterator[j] /= card[i];
    }

    double time_taken = omp_get_wtime();
    while ((iter < maxIter) && (diffErr > minErr)) {
        diffErr = Err;
        Err = 0.0;

        /* 1 - DATA CLASSIFICATION */
        #pragma omp parallel for reduction(+: Err) reduction(+:card[:K]) schedule(dynamic, 5) shared(c) reduction(+:means_non_normalized[:K *(dim + padding)])
        for(unsigned v = 0; v < nbVec; ++v) {
            __m256 *data_iterator = __builtin_assume_aligned(data + v * (dim + padding), 32);

            unsigned char min = 0;
            double distMin = DBL_MAX;

            for(unsigned cluster = 0; cluster < K; ++cluster) {
                __m256 *means_iterator = __builtin_assume_aligned(means + cluster * (dim + padding), 32);
                __m256 dist_accumulator = _mm256_setzero_ps();

                for(unsigned f = 0; f < (dim + padding)/ 8; ++f) {
                    __m256 d = SUB(data_iterator[f], means_iterator[f]);
                    dist_accumulator = ADD(dist_accumulator, MUL(d, d));
                }

                double dist = sum8(dist_accumulator);

                if(dist < distMin) {
                    distMin = dist;
                    min = cluster;
                }
            }

            Err += sqrt(distMin);

            if (c[v] != min)
            {
                float *means_prev_it = means_non_normalized + c[v] * (dim + padding);
                float *means_new_it = means_non_normalized + min * (dim + padding);
                const float *data_it = data + v * (dim + padding);

                #pragma omp simd
                for (unsigned j = 0; j < dim; j++)
                {
                    means_prev_it[j] += -data_it[j];
                    means_new_it[j] += data_it[j];
                }

                card[c[v]] += -1;
                card[min] += 1;

                c[v] = min;
            }
        }

        /* 2 - MEAN UPDATE */

        #pragma omp parallel for
        for (unsigned cluster = 0; cluster < K; cluster++)
        {
            float *means_src_it = __builtin_assume_aligned(means_non_normalized + cluster * (dim + padding), 32);
            float *means_dst_it = __builtin_assume_aligned(means + cluster * (dim + padding), 32);
            #pragma omp simd
            for (unsigned i = 0; i < dim; i++)
                means_dst_it[i] = means_src_it[i] / card[cluster];
        }


        /* 3 - LOOP UPDATE */
        ++iter;
        Err /= nbVec;
        diffErr = fabs(diffErr - Err);
        printf("Iteration: %d, Error: %f\n", iter, Err);
    }
    time_taken = omp_get_wtime() - time_taken;
    printf("Iteration took in average %f seconds to execute \n", time_taken / iter);

    free(means);
    free(means_non_normalized);
    free(card);

    return c;
}

int main(int ac, char *av[])
{
    if (ac != 8) {
        printf("Usage :\n\t%s <K: int> <maxIter: int> <minErr: float> <dim: int> <nbvec:int> <datafile> <outputClassFile>\n", av[0]);
        exit(1);
    }
	unsigned maxIter = atoi(av[2]);
	double minErr = atof(av[3]);
	unsigned K = atoi(av[1]);
	unsigned dim = atoi(av[4]);
	unsigned nbVec = atoi(av[5]);

    printf("Start Kmeans on %s datafile [K = %d, dim = %d, nbVec = %d]\n", av[6], K, dim, nbVec);

    struct kmean_data info = loadData(av[6], nbVec, dim);
    unsigned char * classif = Kmeans(&info, K, minErr, maxIter);
        writeClassinFloatFormat(classif, nbVec, av[7]);

    free(classif);
    free(info.data);

    return 0;
}
