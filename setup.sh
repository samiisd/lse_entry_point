#!/bin/sh
[ $# -ne 1 ] && echo "usage: $0 <path: ember_folder>" && exit 1;

[ ! -d $1 ] && echo "The specified path ('$1') is not a correct folder" &&
    exit 1;

save_curr=$PWD

# Get the absolute path of EMBER folder
cd "$1"
ember_folder="$PWD"
cd "$save_curr"

# Check if the EMBER folder contains all sets
for dataset in Xtrain.dat Ytrain.dat Xtest.dat Ytest.dat; do
    [ ! -e "$ember_folder/$dataset" ] && echo "file
    '$ember_folder/$dataset' not found" && exit 1;
done

# Compile Kmeans
echo "Compiling Kmeans..."
cd Kmeans; make; cd "$save_curr"

# Split the dataset into three parts
echo "Compiling DataSplitter..."
cd EmberClassification/dataset/; make
echo "Splitting the dataset..."
./split_data "$ember_folder/Xtrain.dat" "$ember_folder/Ytrain.dat"

# Create shortcuts
echo "Creating shortcut to testset..."
ln -s "$ember_folder/Xtest.dat" Xtest.dat
ln -s "$ember_folder/Ytest.dat" Ytest.dat

cd $save_curr

echo "All work done!"
