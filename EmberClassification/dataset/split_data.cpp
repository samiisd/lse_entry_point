#include <iostream>
#include <fstream>
#include <string>
using namespace std;

int main (int argc, char *argv[]) {
    
    if (argc != 3)
    {
        cout << "usage: " << argv[0] << " <path: x_train.dat>"
                                        " <path: y_train.dat>"
                                     << endl;

        return 1;
    }

    /* --- Variables declaration --- */
    ifstream x_train, y_train;

    ofstream new_set[3];;

    const size_t nb_vector = 900000;
    const size_t vector_dim = 2351;

    /* --- Initialisation --- */
    x_train.open(argv[1], ios::in | ios::binary);
    y_train.open(argv[2], ios::in | ios::binary);

    new_set[0].open("Xtrain_unlabeled.dat", ios::out | ios::binary);
    new_set[1].open("Xtrain_benign.dat", ios::out | ios::binary);
    new_set[2].open("Xtrain_malicious.dat", ios::out | ios::binary);

    size_t x_train_size = sizeof(float) * nb_vector * vector_dim;
    size_t y_train_size = sizeof(float) * nb_vector;

    float *buffer = new float[vector_dim];
    float *c = new float[y_train_size];

    y_train.read((char*)c, sizeof(float) * nb_vector);
try
{
    if (y_train && (y_train.gcount() / sizeof(float)) == nb_vector)
        cout << "[OK] Ytrain correctly maped..." << endl;
    else
        throw "error : able to read only : " + to_string(y_train.gcount() / sizeof(float)) + " int!";

    auto i = 0;
    while (i < nb_vector) {
        x_train.read((char*)buffer, sizeof(float) * vector_dim);
        if (x_train.gcount() !=  sizeof(float) * vector_dim)
            throw "error : unable to extract the next float (extracted yet : " + to_string(i) + " / " + to_string(nb_vector) + ")";

        new_set[ (int)c[i] + 1].write((char *)buffer, sizeof(float) * vector_dim);

        i++;
    }

    for (auto j = 0; j < 3; j++) {
        if (new_set[j].tellp() != (nb_vector/3) * vector_dim * sizeof(float))
            throw "error : unable to write all";
    }
}
catch (std::exception &e)
{
    cout << "[//] error : " << e.what() << endl;
}
    cout << "[OK] Xtrain correctly splitted into 3 files..." << endl;

    /*  --- Releasing ressources --- */
    delete []buffer;
    delete []c;

    x_train.close();
    y_train.close();

    for (auto j = 0; j < 3; j++)
        new_set[j].close();

    return 0;
}
