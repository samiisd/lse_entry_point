# Projet LSE

Ce repo contient les rapports (et autres...) demandées dans le sujet de recrutement LSE.

## 1. Sécurité

À la racine même du dossier, il y a le rapport intitulé "**./report.pdf**" contenant la description des méthodes de résolutions aux problèmes posés dans le sujet Sécurité.

Vous trouverez dans le dossier "**./Kmeans/**" le code source du KMeans amélioré, ainsi qu'un Makefile pour le compiler. 

## 2. IA

Le rapport de la partie IA a été écrit sous forme de **notebook** pour illustrer plus facilement les propos et expliquer au mieux le code.

Vu la difficulté que j'ai eu à combiner des termes français et anglais lors de la rédaction du rapport Kmeans, j'ai décidé de rediger le notebook en **anglais**.

### Mise en route (**important**)

Avant d'utiliser le notebook, il faut préparer les données.

Pour cela, il suffit d'executer le script "**./setup.sh**". :

    ./setup.sh <path: ember_data_set_folder>

L'opération prend à peu près 5-10 min sur un HDD. (*devrait être plus rapide sur un SSD*)

### Formats du notebook

Vous trouverez le notebook dans le dossier "**./EmberClassification/**" sous deux formats:

+ IPYNB : il faut alors l'ouvrir avec **Jupyter**
+ HTML: au cas où il y aurais des problèmes avec le notebook




# Autheur:

Sami Issaadi, ING1, promo 2021


